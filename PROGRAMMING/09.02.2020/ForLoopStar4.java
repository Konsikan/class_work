public class ForLoopStar4{
	public static void main(String args[]){
		int num=8;
		double x=Math.pow(num,2);
		for(int i=1; i<=x/2; i++){
			System.out.print("*");
			if (i%num==0){
				System.out.println("");
				for(int n=1; n<=num; n++){
					System.out.print("#");
					if (n%num==0){
						System.out.println("");
					}
				}
			}
		}	
	}
}