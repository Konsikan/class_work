public class SwitchCaseForEvenOrOdd{
	public static void main(String args[]){
		int num=12;
		int numx=num%2;
		System.out.println("      "+num);
		switch(numx){
			case 0:
				System.out.println("| EVEN NUMBER |");
				break;
			case 1:
				System.out.println("| ODD NUMBER |");
				break;
			default:
				System.out.println("| INVALID |");
		}	
	}
}