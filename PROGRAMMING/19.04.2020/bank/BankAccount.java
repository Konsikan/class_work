package prog.bcas.bank;

public class BankAccount {

	public static String bankName = "| BANK OF CEYLON |";
	private String branch;
	private double accountBalance = 0;
	private String accountType;
	private String accountNumber;
	private String accountHolderName;

	public void openAccount(String branch, String accountType, String accountNumber, String accountHolderName,
			int amount) {
		this.branch = branch;
		this.accountType = accountType;
		this.accountNumber = accountNumber;
		this.accountHolderName = accountHolderName;
		this.accountBalance = amount;

	}

	public void withdrawal(double amount) {
		accountBalance = accountBalance - amount;

	}

	public void deposit(double amount) {
		accountBalance = accountBalance + amount;
	}

	public void getAccountDetails(String accountNo) {
		System.out.println("----------------------------------------------------------");
		System.out.println("BANK NAME  \t\t: " + bankName + "| " + branch+" |");
		System.out.println("ACCOUNT TYPE  \t\t: " + accountType);
		System.out.println("ACCOUNT NUMBER  \t: " + accountNumber);
		System.out.println("ACCOUNT HOLDER NAME  \t: " + accountHolderName);
		System.out.println("ACCOUNT BALANCE  \t: " + accountBalance);
		System.out.println("----------------------------------------------------------");
	}
}
