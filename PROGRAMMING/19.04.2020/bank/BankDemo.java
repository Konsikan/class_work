package prog.bcas.bank;

public class BankDemo {
	public static void main(String[] args) {

		BankAccount accKonsikan = new BankAccount();
		BankAccount accMathiyalagan = new BankAccount();

		accKonsikan.openAccount("Jaffna", "Saving Account", "89645782", "J.Konsikan", 10000);
		accMathiyalagan.openAccount("Pointpedro", "Current Account", "775896147", "V.Mathiyalagan", 25000);

		accKonsikan.withdrawal(500);
		accMathiyalagan.deposit(50000);

		accKonsikan.getAccountDetails("89645782");
		accMathiyalagan.getAccountDetails("775896147");

		System.out.println(accKonsikan);
		System.out.println(accMathiyalagan);

	}
}

/*
OUTPUT
----------------------------------------------------------
BANK NAME  		: | BANK OF CEYLON || Jaffna |
ACCOUNT TYPE  		: Saving Account
ACCOUNT NUMBER  	: 89645782
ACCOUNT HOLDER NAME  	: J.Konsikan
ACCOUNT BALANCE  	: 9500.0
----------------------------------------------------------
----------------------------------------------------------
BANK NAME  		: | BANK OF CEYLON || Pointpedro |
ACCOUNT TYPE  		: Current Account
ACCOUNT NUMBER  	: 775896147
ACCOUNT HOLDER NAME  	: V.Mathiyalagan
ACCOUNT BALANCE  	: 75000.0
----------------------------------------------------------
*/
