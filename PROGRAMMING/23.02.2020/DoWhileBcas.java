public class DoWhileBcas{
	public static void main(String args[]){
		int i=10;
		int num=0;
		do{
			System.out.println("| BCAS |");
			num=num+1;
		}while(num<=i);
			
	}
}
/*

OUTPUT

| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |

*/