public class SumOfEvenNumber{
	public static void main(String args[]){
		int num=10;
		int sum=0;
		for(int i=1; i<=num; i+=2){
				sum=sum+i;
		}
		System.out.println("SUM OF FIRST "+num+" EVEN NUMBERS = "+ " | "+sum+" |");
	}
}
/*

OUTPUT

SUM OF FIRST 10 EVEN NUMBERS =  | 30 |

*/