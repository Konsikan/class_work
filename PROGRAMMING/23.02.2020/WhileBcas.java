public class WhileBcas{
	public static void main(String args[]){
		int num=1;
		while(num<10){
			System.out.println("| BCAS |");
			num=num+1;
		}	
	}
}
/*

OUTPUT
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |
| BCAS |

*/