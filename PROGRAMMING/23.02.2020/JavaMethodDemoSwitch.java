public class JavaMethodDemoSwitch{
	public void Sum(){
		int i=10;
		int num=1;
		int sum=0;
		while(num<=i){
			sum=sum+num;
			num=num+1;
		}
			System.out.println("SUM OF FIRST "+i+" NATURAL NUMBERS = "+" | "+sum+" |");
		
	}
	public void Factorial(){
		int i=10;
		int num=1;
		int sum=1;
		while(num<=i){
			sum=sum*num;
			num=num+1;
		}
			System.out.println("FACTORIAL OF FIRST "+i+" NATURAL NUMBERS = "+" | "+sum+" |");
	}
	public static void main(String args[]){
		JavaMethodDemoSwitch demo=new JavaMethodDemoSwitch();
		demo.Sum();
		demo.Factorial();
	}
}

/*

OUTPUT

*/