public class DoWhileDecrease{
	public static void main(String args[]){
		int i=10;
		do{
			System.out.println(i);
			i--;
		}while(i>=0);
			
	}
}
/*

OUTPUT

10
9
8
7
6
5
4
3
2
1
0

*/