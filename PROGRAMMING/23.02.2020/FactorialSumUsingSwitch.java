public class FactorialSumUsingSwitch{
	public static void main(String args[]){
		int i=10;
		int num=1;
		int sum=0;
		char f='F';
		switch(f){
			case 'S':
				while(num<=i){
					sum=sum+num;
					num=num+1;
				}
					System.out.println("SUM OF FIRST "+i+" NATURAL NUMBERS = "+" | "+sum+" |");
					break;
			case 'F':
				sum=1;
				while(num<=i){
					sum=sum*num;
					num=num+1;
				}
					System.out.println("FACTORIAL OF FIRST "+i+" NATURAL NUMBERS = "+" | "+sum+" |");
					break;
			default:
					System.out.println("INVALID");
			}
			
	}
}
/*

OUTPUT
SUM OF FIRST 10 NATURAL NUMBERS =  | 55 |
OR
FACTORIAL OF FIRST 10 NATURAL NUMBERS =  | 3628800 |
OR
INVALID
*/