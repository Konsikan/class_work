public class SumEvenNumberWhile{
	public static void main(String args[]){
		int i=10;
		int num=0;
		int sum=0;
		while(num<=i){
			sum=sum+num;
			num=num+2;
		}
		System.out.println("SUM OF FIRST "+i+" EVEN NUMBERS = "+" | "+sum+" |");	
	}
}
/*

OUTPUT
SUM OF FIRST 10 EVEN NUMBERS =  | 30 |

*/