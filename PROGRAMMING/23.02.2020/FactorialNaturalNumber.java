public class FactorialNaturalNumber{
	public static void main(String args[]){
		int num=5;
		int sum=1;
		for(int i=1; i<=num; i++){
				sum=sum*i;
		}
		System.out.println("FACTORIAL OF FIRST "+num+" NATURAL NUMBERS = "+ " | "+sum+" |");
	}
}
/*

OUTPUT

FACTORIAL OF FIRST 10 NATURAL NUMBERS =  | 120 |

*/