public class JavaMethodDemo{
	public void SayHello(){
		System.out.println("HELLO");		
	}
	public void SayWelcome(){
		System.out.println("WELCOME");
	}
	public static void main(String args[]){
		JavaMethodDemo demo=new JavaMethodDemo();
		demo.SayHello();
		demo.SayWelcome();
	}
}

/*

OUTPUT

HELLO
WELCOME

*/