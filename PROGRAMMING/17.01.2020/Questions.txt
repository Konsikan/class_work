Write the algorithm for the following :
Q1) Find the sum of two numbers
Q2) Convert the temperature from Celsius to Fahrenheit
	(y × 9/5) + 32 = z°F
Q3) Find Area and Perimeter of Square
Q4) Find Area and Perimeter of Rectangle
Q5) Draw Flowchart to find the Area and Perimeter of Circle
Q6) Find Area & Perimeter of Triangle
Q7) Find Simple Interest
 P: Principal Amount
N: Time in Years
R: % Annual Rate of Interest
SI: Simple Interest
Q8) Swap Two Numbers using a temporary variable
Q9) Swap Two Numbers without using a temporary variable
Q10) Find the smallest of two numbers
Q11) Find the largest of two numbers
Q12) Find the largest of three numbers
Q13) Find Even number between 1 to 50
Q14) Find the sum of series 1+2+3+.....+N
Q15) Find sum and average of given series of numbers