public class CalculatorDemo{
		public static void main(String args[]){
					Calculator demo=new Calculator();
					int num1=20;
					int num2=5;
					demo.addNum(num1,num2);
					demo.subNum(num1,num2);
					demo.adNum(num1,num2);
					demo.divNum(num1,num2);
					demo.modNum(num1,num2);
		}
}
 /* 
 OUTPUT
 
ADDITION OF             20 + 5   | 25
SUBTRACTION OF          20 + 5   | 15
MULTIPLICATION OF       20 + 5   | 100
DIVITION OF             20 / 5   | 4
MOD OF                  20 % 5   | 0

*/