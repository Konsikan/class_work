public class SwapTwoNumbers{
	public static void main(String args[]){
		int num1=20;
		int num2=30;
		System.out.println("Before Swapping");
		System.out.println("Num1 = "+ num1 + " | Num2 = "+ num2);
		int x=num1;
		 num1=num2;
		 num2=x;
		System.out.println("After Swapping");
		System.out.println("Num1 = "+ num1 + " | Num2 = "+ num2);
	}
}