public class Triangle{
	public static void main(String args[]){
		int side1=15;
		int side2=12;
		int side3=20;
		int base=15;
		int height=20;
		int perimeter=side1+side2+side3;
		double area=1.0/2.0*base*height;
		System.out.println("Side1 = "+side1+" | Side2 = "+side2+" | Side3 = "+side3);
		System.out.println("Base = "+base+" | Height = "+height);
		System.out.println("Area = "+area+" Perimeter = "+perimeter);
	}
}