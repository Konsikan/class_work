public class SwapTwoNumbers2{
	public static void main(String args[]){
		int num1=20;
		int num2=30;
		System.out.println("Before Swapping");
		System.out.println("Num1 = "+ num1 + " | Num2 = "+ num2);
		 num1=num1+num2;
		 num2=num1-num2;
		 num1=num1-num2;
		System.out.println("After Swapping");
		System.out.println("Num1 = "+ num1 + " | Num2 = "+ num2);
	}
}