public class BillAmount{
	public static void main(String args[]){
		int unit=100;
		System.out.println("");
		System.out.println("   CONSUMPTION     COST PER UNIT");
		System.out.println("|0-30      UNITS |  Rs  3.00");
		System.out.println("|31-60     UNITS |  Rs  5.00");
		System.out.println("|61-90     UNITS |  Rs 10.00");
		System.out.println("|91-120    UNITS |  Rs 20.00");
		System.out.println("|121-180   UNITS |  Rs 30.00");
		System.out.println("|181-210   UNITS |  Rs 35.00");
		System.out.println("|211-300   UNITS |  Rs 40.00");
		System.out.println("|ABOVE 300 UNITS |  Rs 50.00");
		System.out.println("");
		System.out.println("|"+"YOUR UNIT-"+unit+"|");
		System.out.println("");
		if(unit>=0){
			int p1=3*30;
			int p2=5*30;
			int p3=10*30;
			int p4=20*30;
			int p5=30*60;
			int p6=35*30;
			int p7=40*30;
			int p8=50*30;
			if (unit<=30){
				int cost=(unit*3);
			}else if ((unit>=31)&&(unit<=60)){
				int cost=(30*3)+(unit-30)*5;
			}else if ((unit>=61)&&(unit<=90)){
				int cost=(30*3)+(30*5)+(unit-60)*10;
			}else if ((unit>=91)&&(unit<=120)){
				int cost=(30*3)+(30*5)+(30*10)+(unit-90)*20;
			}else if ((unit>=121)&&(unit<=180)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(unit-120)*30;
			}else if ((unit>=181)&&(unit<=210)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(unit-180)*35;
			}else if ((unit>=211)&&(unit<=300)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(unit-210)*40;
			}else{
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(90*40)+(unit-300)*50;
			}
			System.out.println("TOTAL COST FOR YOUR UNIT : Rs "+cost+".00");
		}else {
			System.out.println("ERROR");
			
		
	}
}